<?php


namespace common\Services;


abstract class IHistoryDataProvider
{
    public abstract function GetHistoryByQuery($userId, $queryType, $yearFrom, $monthFrom, $dayFrom, $yearTo, $monthTo, $dayTo, $place, $type, $knowledgeId, $unlimitedResult );

    public abstract function GetHistoryParams($UserId);
}
