<?php


namespace common\Services;


use api\modules\v1\Tables\Knowledgearticleunit;
use api\modules\v1\Tables\Knowledgevideounit;

class VideoDataBaseProvider extends IVideoDataProvider
{
    public function GetVidoByUserID($id, $limit)
    {
        return Knowledgevideounit::find()->orderBy("dateofstudy DESC")->limit($limit)->all();
    }

    public function GetVideoByQuery($id, $limit, $dateFrom, $dateTo, $place, $knowledgeId)
    {
        $dateStart = null;
        $dateEnd = null;
        if ($dateFrom != null && $dateTo != null){
            $dateStart = $dateFrom->format('Y-m-d');
            $dateEnd = $dateTo->format('Y-m-d');
        }

        return Knowledgevideounit::find()->filterWhere(['between', 'dateofstudy',  $dateStart, $dateEnd])->
        andFilterWhere(['placeofstudy' => $place, 'knowledge' => $knowledgeId])->orderBy("dateofstudy DESC")->limit($limit)->all();
    }
}
