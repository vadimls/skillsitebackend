<?php

namespace common\Services;

use api\modules\v1\models;

abstract class ILeftMenuDataProvider
{
    public abstract function GetLeftMenuModelByUserID($userId);
}
