<?php


namespace common\Services;


abstract class IBookPartDataProvider
{
    public abstract function GetBookPartByUserID($id, $limit);

    public abstract function GetAllBookByUserID($id);

    public abstract function GetBookPartByQuery($id, $limit, $dateFrom, $dateTo, $place, $knowledgeId);
}
