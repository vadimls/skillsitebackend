<?php

namespace common\Services;

use api\modules\v1\models\LeftMenuModel;
use Yii;

class LeftMenuDatabaseRealtimeCalculateProvider extends ILeftMenuDataProvider
{
    private  $knowledgeProvider;

    public function GetLeftMenuModelByUserID($userId)
    {
        $lf = new LeftMenuModel;
        $lf->FullName = "Лысов Вадим";

        $this->knowledgeProvider = new KnowledgeDatabaseProvider;
        $knowleadgebyuser = $this->knowledgeProvider->GetAllKnowledgeByUserID($userId);

        //TODO: Установить какой нибудь аналог Linq как в C#, например, YaLinqo и переписать код под него.
        foreach ($knowleadgebyuser as $oneKnowlead){

            foreach ($oneKnowlead->knowledgearticleunits as $oneArticle){
                $lf->ArticleCount++;
                $lf->TotalPoint += $oneArticle->MoneyCount;
            }
            foreach ($oneKnowlead->knowledgevideounits as $oneVideo){
                $lf->VideoCount++;
                $lf->TotalPoint += $oneVideo->MoneyCount;
            }
            foreach ($oneKnowlead->knowledgebookunitparts as $oneBookPart){
                $lf->TotalPoint += $oneBookPart->MoneyCount;
            }
            $lf->BookCount+=count($oneKnowlead->knowledgebookunits);
        }
        return $lf;
    }
}
