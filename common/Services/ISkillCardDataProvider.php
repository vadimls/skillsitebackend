<?php

namespace common\Services;

use api\modules\v1\models;

abstract class ISkillCardDataProvider
{
    public abstract function GetAllSkillCardByUserID($userId);

    public abstract function GetSkillRowBySkillId($skillId);

    public abstract function GetAllSkillRowByUserId($userId, $limit);
}
