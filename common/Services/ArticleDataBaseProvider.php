<?php


namespace common\Services;

use api\modules\v1\Tables\Knowledgearticleunit;

class ArticleDataBaseProvider extends IArticleDataProvider
{

    public function GetArticleByUserID($id, $limit)
    {
       return Knowledgearticleunit::find()->orderBy("dateofstudy DESC")->limit($limit)->all();
    }

    public function GetArticleByQueryParams($id, $limit, $dateFrom, $dateTo, $place, $knowledgeId)
    {
        $dateStart = null;
        $dateEnd = null;
        if ($dateFrom != null && $dateTo != null){
            $dateStart = $dateFrom->format('Y-m-d');
            $dateEnd = $dateTo->format('Y-m-d');
        }


        return Knowledgearticleunit::find()->filterWhere(['between', 'dateofstudy',  $dateStart, $dateEnd])->
                        andFilterWhere(['placeofstudy' => $place, 'knowledge' => $knowledgeId])->orderBy("dateofstudy DESC")->limit($limit)->all();
    }
}

