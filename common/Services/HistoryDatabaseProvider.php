<?php


namespace common\Services;

use api\modules\v1\models\HistoryParam;
use api\modules\v1\models\LeftMenuModel;
use api\modules\v1\models\SkillRowModel;
use api\modules\v1\Tables\Knowledge;
use DateTime;
use Yii;


class HistoryDatabaseProvider extends IHistoryDataProvider
{
    const LimitOfRow = 1000;
    private $ArticleDataProvider;
    private $KnowledgeDataProvider;
    private $VideoDataProvider;
    private $BookPartDataProvider;

    public function __construct()
    {
        //TODO: переделать под DI
        $this->ArticleDataProvider = new ArticleDataBaseProvider();
        $this->KnowledgeDataProvider = new KnowledgeDatabaseProvider();
        $this->VideoDataProvider = new VideoDataBaseProvider();
        $this->BookPartDataProvider = new BookPartDataBaseProvider();
    }

    public function GetHistoryParams($UserId)
    {
        $HistoryParams = array();
        array_push($HistoryParams, new HistoryParam("","Все источники","type"));
        array_push($HistoryParams, new HistoryParam("Видео","Видео","type"));
        array_push($HistoryParams, new HistoryParam("Книга","Книга","type"));
        array_push($HistoryParams, new HistoryParam("Статья","Статья","type"));

        array_push($HistoryParams, new HistoryParam("","Все места","place"));
        array_push($HistoryParams, new HistoryParam("дома","дома","place"));
        array_push($HistoryParams, new HistoryParam("на работе","на работе","place"));
        array_push($HistoryParams, new HistoryParam("в дороге","в дороге","place"));

        array_push($HistoryParams, new HistoryParam("","Все навыки","skill"));
        $AllKnowledges = $this->KnowledgeDataProvider->GetAllKnowledgeByUserIDwithoutAdditionalFields($UserId);
        foreach ($AllKnowledges as $oneKnowlead ){
            array_push($HistoryParams, new HistoryParam($oneKnowlead->Id,$oneKnowlead->Title,"skill"));
        }

        return $HistoryParams;
    }

    public function GetHistoryByQuery($userId, $queryType, $yearFrom, $monthFrom, $dayFrom, $yearTo, $monthTo, $dayTo, $place, $type, $knowledgeId, $unlimitedResult )
    {
        $PathToIcons = $this->GetPathToIcons($userId);
        $SkillTitles = $this->GetKnowledgesTitle($userId);
        $BooksName = $this->GetBooksName($userId);

        $SkilRowModelbyUser = array();
        $IndexSkilRowArray = 0;

        $datefrom = $this->GetDateFromString($yearFrom, $monthFrom, $dayFrom);
        $dateTo = $this->GetDateFromString($yearTo, $monthTo, $dayTo);
        $currentLimit = $unlimitedResult ? 999999 : $this::LimitOfRow;

        if (empty($type) || $type == "Статья"){
            $articleRow = $this->ArticleDataProvider->GetArticleByQueryParams($userId, $currentLimit, $datefrom,$dateTo, $place,$knowledgeId);
            foreach ($articleRow as $oneArticle){
                $SkilRowModelbyUser[$IndexSkilRowArray++] = $this->copyRecord($oneArticle,"ArticleRow",$PathToIcons[$oneArticle->Knowledge],$oneArticle->Title, $SkillTitles[$oneArticle->Knowledge]);
            }
        }
        if (empty($type) || $type == "Видео"){
            $videoRow = $this->VideoDataProvider->GetVideoByQuery($userId, $currentLimit, $datefrom,$dateTo, $place,$knowledgeId);
            foreach ($videoRow as $oneVideo){
                $SkilRowModelbyUser[$IndexSkilRowArray++] = $this->copyRecord($oneVideo,"VideoRow",$PathToIcons[$oneVideo->Knowledge], $oneVideo->Title, $SkillTitles[$oneVideo->Knowledge]);
            }
        }
        if (empty($type) || $type == "Книга") {
            $bookPartRow = $this->BookPartDataProvider->GetBookPartByQuery($userId, $currentLimit, $datefrom,$dateTo, $place,$knowledgeId);
            foreach ($bookPartRow as $oneBookPart){
                $SkilRowModelbyUser[$IndexSkilRowArray++] = $this->copyRecord($oneBookPart,"BookRow",$PathToIcons[$oneBookPart->Knowledge],$BooksName[$oneBookPart->OwnerBook], $SkillTitles[$oneBookPart->Knowledge] );
            }
        }

        usort($SkilRowModelbyUser, function ($a,$b){
            if ($a->DateStudy == $b->DateStudy) {
                return 0;
            }
            return ($a->DateStudy < $b->DateStudy) ? 1 : -1;
        });

        return array_slice($SkilRowModelbyUser,0,$currentLimit);
    }

    private function copyRecord($targetRow, $rowType, $pathToIcon, $title, $knowledgeTitle){
        $row = new SkillRowModel();
        $row->Id = $targetRow->Id;
        $row->Title = $title;
        $row->DateStudy = $targetRow->DateOfStudy;
        $row->PlaceOfStudy = $targetRow->PlaceOfStudy;
        $row->MoneyCount = $targetRow->MoneyCount;
        $row->RowType = $rowType;
        $row->PathToIcon = $pathToIcon;
        $row->SkillName = $knowledgeTitle;
        return $row;
    }

    private function GetPathToIcons($id){
        $AllKnowledges = $this->KnowledgeDataProvider->GetAllKnowledgeByUserIDwithoutAdditionalFields($id);
        $PathToIcons = array();
        foreach ($AllKnowledges as $oneKnowlead ){
            $PathToIcons[$oneKnowlead->Id] = $oneKnowlead->PathToIcon;
        }
        return $PathToIcons;
    }

    private function GetKnowledgesTitle($id){
        $AllKnowledges = $this->KnowledgeDataProvider->GetAllKnowledgeByUserIDwithoutAdditionalFields($id);
        $Titles = array();
        foreach ($AllKnowledges as $oneKnowlead ){
            $Titles[$oneKnowlead->Id] = $oneKnowlead->Title;
        }
        return $Titles;
    }

    private function GetBooksName($id){
        $AllBooks = $this->BookPartDataProvider->GetAllBookByUserID($id);
        $BooksName = array();
        foreach ($AllBooks  as $oneBook ){
            $BooksName[$oneBook->Id] = $oneBook->Title;
        }
        return $BooksName;
    }

    private function GetDateFromString($year, $month, $day){
        $datefrom = null;
        if ($year != null & $month != null & $day != null){
            $datefrom = new DateTime();
            $datefrom->setDate($year, $month, $day);
        }
        return $datefrom;
    }
}
