<?php


namespace common\Services;


abstract class IVideoDataProvider
{
    public abstract function GetVidoByUserID($id, $limit);

    public abstract function GetVideoByQuery($id, $limit, $dateFrom, $dateTo, $place, $knowledgeId);
}
