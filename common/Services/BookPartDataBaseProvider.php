<?php


namespace common\Services;


use api\modules\v1\Tables\Knowledgebookunit;
use api\modules\v1\Tables\Knowledgebookunitpart;
use api\modules\v1\Tables\Knowledgevideounit;

class BookPartDataBaseProvider extends IBookPartDataProvider
{
    public function GetBookPartByUserID($id, $limit)
    {
        return Knowledgebookunitpart::find()->orderBy("dateofstudy DESC")->limit($limit)->all();
    }

    public function GetAllBookByUserID($id)
    {
         return Knowledgebookunit::find()->all();
    }

    public function GetBookPartByQuery($id, $limit, $dateFrom, $dateTo, $place, $knowledgeId)
    {
        $dateStart = null;
        $dateEnd = null;
        if ($dateFrom != null && $dateTo != null){
            $dateStart = $dateFrom->format('Y-m-d');
            $dateEnd = $dateTo->format('Y-m-d');
        }

        return Knowledgebookunitpart::find()->filterWhere(['between', 'dateofstudy',  $dateStart, $dateEnd])->
        andFilterWhere(['placeofstudy' => $place, 'knowledge' => $knowledgeId])->orderBy("dateofstudy DESC")->limit($limit)->all();
    }
}
