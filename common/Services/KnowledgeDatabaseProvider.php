<?php


namespace common\Services;

use yii\debug\models\search\Db;
use api\modules\v1\Tables\Knowledge;

class KnowledgeDatabaseProvider extends IKnowledgeDataProvider
{
    public function GetAllKnowledgeByUserID($id)
    {
        return Knowledge::find()->with('knowledgearticleunits',
            'knowledgevideounits', 'knowledgebookunitparts','knowledgebookunits')->all();
    }

    public function GetKnowledgeByID($id)
    {
        return Knowledge::findOne($id);
    }

    public function GetAllKnowledgeByUserIDwithoutAdditionalFields($id)
    {
        return Knowledge::find()->all();
    }
}
