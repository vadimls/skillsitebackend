<?php


namespace common\Services;

use api\modules\v1\models\SkilCardModel;
use api\modules\v1\models\SkillRowModel;

class SkillCardRealtimeCalculateDataProvider extends ISkillCardDataProvider
{

    private  $knowledgeProvider;

    public function __construct()
    {
        //TODO: переделать под DI
        $this->knowledgeProvider = new KnowledgeDatabaseProvider();
    }

    public function GetAllSkillCardByUserID($userId)
    {
        $knowleadgebyuser = $this->knowledgeProvider->GetAllKnowledgeByUserID($userId);

        $SkilCardModelbyUser = array();

        foreach ($knowleadgebyuser as $key=>$oneKnowleadg){

            $SkilCard = new SkilCardModel();
            $SkilCard->Id = $oneKnowleadg->Id;
            $SkilCard->Title = $oneKnowleadg->Title;
            $SkilCard->PathToIcon = $oneKnowleadg->PathToIcon;
            $SkilCard->TheoreticalExperience = $oneKnowleadg->TheoreticalExperience;
            $SkilCard->PracticalExperience = $oneKnowleadg->PracticalExperience;
     //       $SkilCard->MoneyCount = $oneKnowleadg->MoneyCount;
            $SkilCard->BookCount = count($oneKnowleadg->knowledgebookunits);
            $SkilCard->ArticleCount = count($oneKnowleadg->knowledgearticleunits);
            $SkilCard->VideoCount = count($oneKnowleadg->knowledgevideounits);

            foreach ($oneKnowleadg->knowledgearticleunits as $oneArticle){
                $SkilCard->MoneyCount += $oneArticle->MoneyCount;
            }
            foreach ($oneKnowleadg->knowledgevideounits as $oneVideo){
                $SkilCard->MoneyCount += $oneVideo->MoneyCount;
            }
            foreach ($oneKnowleadg->knowledgebookunitparts as $oneBookPart){
                $SkilCard->MoneyCount += $oneBookPart->MoneyCount;
            }

            $SkilCardModelbyUser[$key] = $SkilCard;
        }
        return $SkilCardModelbyUser;

    }

    public function GetSkillRowBySkillId($skillId)
    {
        $this->knowledgeProvider = new KnowledgeDatabaseProvider;
        $knowleadgebyuser = $this->knowledgeProvider->GetKnowledgeByID($skillId);

        $SkilRowModelbyUser = array();
        $IndexSkilRowArray = 0;

        foreach ($knowleadgebyuser->knowledgearticleunits as $oneArticle){
            $row = new SkillRowModel();
            $row->Id = $oneArticle->Id;
            $row->Title = $oneArticle->Title;
            $row->DateStudy = $oneArticle->DateOfStudy;
            $row->PlaceOfStudy = $oneArticle->PlaceOfStudy;
            $row->MoneyCount = $oneArticle->MoneyCount;
            $row->RowType = "ArticleRow";
            $SkilRowModelbyUser[$IndexSkilRowArray++] = $row;
        }
        foreach ($knowleadgebyuser->knowledgevideounits as $oneVideo){
            $row = new SkillRowModel();
            $row->Id = $oneVideo->Id;
            $row->Title = $oneVideo->Title;
            $row->DateStudy = $oneVideo->DateOfStudy;
            $row->PlaceOfStudy = $oneVideo->PlaceOfStudy;
            $row->MoneyCount = $oneVideo->MoneyCount;
            $row->RowType = "VideoRow";
            $SkilRowModelbyUser[$IndexSkilRowArray++] = $row;
        }
        foreach ($knowleadgebyuser->knowledgebookunits as $oneBook){
            $row = new SkillRowModel();
            $row->Id = $oneBook->Id;
            $row->Title = $oneBook->Title;
            $row->DateStudy = $oneBook->DateOfStudy;
            $row->PathToIcon = $oneBook->PathToIcon;
            $row->PlaceOfStudy = $oneBook->PlaceOfStudy;
            $row->MoneyCount = $oneBook->MoneyCount;
            $row->RowType = "BookRow";
            $SkilRowModelbyUser[$IndexSkilRowArray++] = $row;
        }

        usort($SkilRowModelbyUser, function ($a,$b){
            if ($a->DateStudy == $b->DateStudy) {
                return 0;
            }
            return ($a->DateStudy < $b->DateStudy) ? 1 : -1;
        });

        return $SkilRowModelbyUser;
    }

    public function GetAllSkillRowByUserId($userId, $limit)
    {

    }
}
