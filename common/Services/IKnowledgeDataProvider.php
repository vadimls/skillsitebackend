<?php


namespace common\Services;


abstract class IKnowledgeDataProvider
{
    public abstract function GetAllKnowledgeByUserID($id);

    public abstract function GetAllKnowledgeByUserIDwithoutAdditionalFields($id);

    public abstract function GetKnowledgeByID($id);


}
