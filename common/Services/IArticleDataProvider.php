<?php


namespace common\Services;


abstract class IArticleDataProvider
{
    public abstract function GetArticleByUserID($id, $limit);

    public abstract function GetArticleByQueryParams($id, $limit, $datefrom, $dateTo, $place, $knowledgeId);

}
