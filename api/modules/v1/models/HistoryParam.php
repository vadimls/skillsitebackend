<?php


namespace api\modules\v1\models;


class HistoryParam
{
    public $Id = "";

    public $Name = "";

    public $Type = "";

    public function __construct($Id ,$Name ,$Type)
    {
        $this->Id = $Id;
        $this->Name = $Name;
        $this->Type = $Type;
    }
}
