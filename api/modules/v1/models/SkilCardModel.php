<?php


namespace api\modules\v1\models;


use yii\db\ActiveRecord;

class SkilCardModel
{
    public $Id;
    public $Title;
    public $PathToIcon;
    public $TheoreticalExperience;
    public $PracticalExperience;
    public $VideoCount;
    public $BookCount;
    public $ArticleCount;
    public $MoneyCount = 0;
}
