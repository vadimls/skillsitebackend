<?php


namespace api\modules\v1\models;


class SkillRowModel
{
    public $Id;
    public $Title;
    public $PathToIcon;
    public $DateStudy;
    public $RowType;
    public $PlaceOfStudy;
    public $MoneyCount;
    public $SkillName;
}
