<?php


namespace api\modules\v1\Tables;


use yii\db\ActiveRecord;

class Knowledgebookunit extends ActiveRecord
{
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return [];
    }

    public $Id;
    public $Title;
    public $PathToIcon;
    public $NumberOfPages;
    public $DaysOfStudy;
    public $Knowledge;
    public $MoneyCount;
    public $DateOfStudy;
    public $PlaceOfStudy;

    public function getKnowledge()
    {
        return $this->hasOne(Knowledge::class, ['Id' => 'Knowledge']);
    }
    public function getKnowledgebookunitparts()
    {
        return $this->hasMany(Knowledgebookunitpart::class, ['OwnerBook' => 'Id']);
    }
}
