<?php


namespace api\modules\v1\Tables;


use yii\db\ActiveRecord;

class Knowledgevideounit extends ActiveRecord
{
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return [];
    }

    public $Id;
    public $Title;
    public $Knowledge;
    public $MoneyCount;
    public $DurationInMinutes;
    public $DateOfStudy;
    public $PlaceOfStudy;

    public function getKnowledge()
    {
        return $this->hasOne(Knowledge::class, ['Id' => 'Knowledge']);
    }
}
