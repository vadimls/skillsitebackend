<?php


namespace api\modules\v1\Tables;


use yii\db\ActiveRecord;

class Knowledgebookunitpart extends ActiveRecord
{
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return [];
    }

    public $Id;
    public $OwnerBook;
    public $PlaceOfStudy;
    public $DateOfStudy;
    public $MoneyCount;
    public $NumberOfPages;
    public $Knowledge;

    public function getKnowledge()
    {
        return $this->hasOne(Knowledge::class, ['Id' => 'Knowledge']);
    }
    public function getKnowledgebookunit()
    {
        return $this->hasMany(Knowledgebookunit::class, ['OwnerBook' => 'Id']);
    }

}
