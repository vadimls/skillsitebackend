<?php

namespace api\modules\v1\Tables;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "image".
 *
 * @property integer $Id
 * @property string $Title
 */
class Knowledge extends ActiveRecord
{
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return [];
    }

    public $Id;
    public $Title;
    public $PathToIcon;
    public $TheoreticalExperience;
    public $PracticalExperience;
    public $MoneyCount;

    public function getKnowledgearticleunits()
    {
        return $this->hasMany(Knowledgearticleunit::class, ['Knowledge' => 'Id']);
    }

    public function KnowledgearticleunitsCount()
    {
        return $this->hasMany(Knowledgearticleunit::class, ['Knowledge' => 'Id'])->count();
    }

    public function getKnowledgebookunits()
    {
        return $this->hasMany(Knowledgebookunit::class, ['Knowledge' => 'Id']);
    }

    public function KnowledgebookunitsCount()
    {
        return $this->hasMany(Knowledgebookunit::class, ['Knowledge' => 'Id'])->count();
    }

    public function getKnowledgevideounits()
    {
        return $this->hasMany(Knowledgevideounit::class, ['Knowledge' => 'Id']);
    }

    public function KnowledgevideounitsCount()
    {
        return $this->hasMany(Knowledgevideounit::class, ['Knowledge' => 'Id'])->count();
    }

    public function getKnowledgebookunitparts()
    {
        return $this->hasMany(Knowledgebookunitpart::class, ['Knowledge' => 'Id']);
    }
}
