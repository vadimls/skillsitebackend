<?php


namespace api\modules\v1\controllers;


use api\modules\v1\models\User;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\Controller;

class BasicauthenticationloginController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function($username,$password){
                if($user = User::findOne(['username' => $username]) and $user->validatePassword($password)){
                    return $user;
                }
                else{
                    return null;
                }
            }
        ];
        return $behaviors;
    }

    public function actionIndex()
    {

    }
}