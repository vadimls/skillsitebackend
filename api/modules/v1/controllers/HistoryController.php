<?php


namespace api\modules\v1\controllers;

use api\common\components\ApiController;
use common\Services\HistoryDatabaseProvider;
use common\Services\ILeftMenuDataProvider;
use common\Services\LeftMenuDatabaseRealtimeCalculateProvider;
use yii\web\UnauthorizedHttpException;
use yii\rest\Controller;
use api\modules\v1\models\LeftMenuModel;
use Yii;

class HistoryController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
//        $auth = $behaviors['authenticator'];
//        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        // re-add authentication filter
//        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
//        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    private $HistoryDataProvider;

    public function __construct($id, $module, $config = [])
    {
        //TODO: переделать под DI
        parent::__construct($id, $module, $config);
        $this->HistoryDataProvider = new HistoryDatabaseProvider();
    }

    public function actionIndex()
    {
        $userId = 0;
        $queryType = $this->TryGetParam('queryType');
        if($queryType == "GetHistoryParams")
           return $this->HistoryDataProvider->GetHistoryParams($userId);

        $yearFrom = $this->TryGetParam('yearFrom');
        $monthFrom = $this->TryGetParam('monthFrom');
        $dayFrom = $this->TryGetParam('dayFrom');
        $yearTo = $this->TryGetParam('yearTo');
        $monthTo = $this->TryGetParam('monthTo');
        $dayTo = $this->TryGetParam('dayTo');
        $place = $this->TryGetParam('place');
        $type = $this->TryGetParam('type');
        $knowledgeId = $this->TryGetParam('knowledgeId');
        $unlimitedResult = $this->TryGetParam('unlimitedResult') == null ? false : $this->TryGetParam('unlimitedResult') == "true" ? true : false;

        return $this->HistoryDataProvider->GetHistoryByQuery($userId, $queryType, $yearFrom, $monthFrom, $dayFrom, $yearTo, $monthTo, $dayTo, $place, $type, $knowledgeId, $unlimitedResult);
    }

    private function TryGetParam($paramName){
        try {
            return $_GET[$paramName];
        }
        catch (\Exception $ex){
            return null;
        }
    }
}
