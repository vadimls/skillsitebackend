<?php

namespace api\modules\v1\controllers;

use api\common\components\ApiController;
use api\modules\v1\models\SkilCardModel;
use common\Services\SkillCardRealtimeCalculateDataProvider;
use yii\web\UnauthorizedHttpException;
use yii\rest\Controller;
use api\modules\v1\models\LeftMenuModel;

class SkilrowController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
//        $auth = $behaviors['authenticator'];
//        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        // re-add authentication filter
//        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
//        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    private $ISkillCardDataProvider;

    public function __construct($id, $module, $config = [])
    {
        //TODO: переделать под DI
        parent::__construct($id, $module, $config);
        $this->ISkillCardDataProvider = new SkillCardRealtimeCalculateDataProvider();
    }

    public function actionIndex($skilid)
    {
        return $this->ISkillCardDataProvider->GetSkillRowBySkillId($skilid);
    }
}
