<?php


namespace api\modules\v1\controllers;

use api\common\components\ApiController;
use api\modules\v1\models\User;
use common\Services\ILeftMenuDataProvider;
use common\Services\LeftMenuDatabaseRealtimeCalculateProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use yii\rest\Controller;
use api\modules\v1\models\LeftMenuModel;
use Yii;

class LeftmenuController extends Controller
{
    private $leftMenuDataProvider;

    public function __construct($id, $module, $config = [])
    {
        //TODO: переделать под DI
        parent::__construct($id, $module, $config);
        $this->leftMenuDataProvider = new LeftMenuDatabaseRealtimeCalculateProvider();
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->leftMenuDataProvider->GetLeftMenuModelByUserID(null);
    }
}
