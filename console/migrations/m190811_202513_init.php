<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190811_202513_init
 */
class m190811_202513_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('knowledge', [
            'Id' => Schema::TYPE_PK,
            'Title' => Schema::TYPE_STRING,
            'PathToIcon' => Schema::TYPE_STRING,
            'TheoreticalExperience' => Schema::TYPE_INTEGER,
            'PracticalExperience' => Schema::TYPE_INTEGER,
            'MoneyCount' => Schema::TYPE_INTEGER,
        ]);
        // creates index for column `author_id`
        $this->createIndex(
            'idx-knowledge-Id',
            'knowledge',
            'Id'
        );

        $this->createTable('knowledgearticleunit', [
            'Id' => Schema::TYPE_PK,
            'Title' => Schema::TYPE_STRING,
            'Knowledge' => Schema::TYPE_INTEGER,
            'MoneyCount' => Schema::TYPE_INTEGER,
            'DateOfStudy' => Schema::TYPE_DATE,
            'PlaceOfStudy' => Schema::TYPE_STRING,
            'foreign key (Knowledge) references `Knowledge` (Id) on delete set null on update cascade'
        ]);
        // creates index for column `author_id`
        $this->createIndex(
            'idx-knowledgearticleunit-Id',
            'knowledgearticleunit',
            'Id'
        );

        $this->createTable('knowledgebookunit', [
            'Id' => Schema::TYPE_PK,
            'Title' => Schema::TYPE_STRING,
            'PathToIcon' => Schema::TYPE_STRING,
            'NumberOfPages' => Schema::TYPE_INTEGER,
            'DaysOfStudy' => Schema::TYPE_INTEGER,
            'Knowledge' => Schema::TYPE_INTEGER,
            'MoneyCount' => Schema::TYPE_INTEGER,
            'DateOfStudy' => Schema::TYPE_DATE,
            'PlaceOfStudy' => Schema::TYPE_STRING,
            'foreign key (Knowledge) references `Knowledge` (Id) on delete set null on update cascade'
        ]);
        // creates index for column `author_id`
        $this->createIndex(
            'idx-knowledgebookunit-Id',
            'knowledgebookunit',
            'Id'
        );

        $this->createTable('knowledgebookunitpart', [
            'Id' => Schema::TYPE_PK,
            'NumberOfPages' => Schema::TYPE_INTEGER,
            'Knowledge' => Schema::TYPE_INTEGER,
            'OwnerBook' => Schema::TYPE_INTEGER,
            'MoneyCount' => Schema::TYPE_INTEGER,
            'DateOfStudy' => Schema::TYPE_DATE,
            'PlaceOfStudy' => Schema::TYPE_STRING,
            'foreign key (Knowledge) references `Knowledge` (Id) on delete set null on update cascade',
            'foreign key (OwnerBook) references `KnowledgeBooKUnit` (Id) on delete set null on update cascade',
        ]);
        // creates index for column `author_id`
        $this->createIndex(
            'idx-knowledgebookunitpart-Id',
            'knowledgebookunitpart',
            'Id'
        );

        $this->createTable('knowledgevideounit', [
            'Id' => Schema::TYPE_PK,
            'Title' => Schema::TYPE_STRING,
            'Knowledge' => Schema::TYPE_INTEGER,
            'MoneyCount' => Schema::TYPE_INTEGER,
            'DurationInMinutes' => Schema::TYPE_INTEGER,
            'DateOfStudy' => Schema::TYPE_DATE,
            'PlaceOfStudy' => Schema::TYPE_STRING,
            'foreign key (Knowledge) references `Knowledge` (Id) on delete set null on update cascade'
        ]);
        // creates index for column `author_id`
        $this->createIndex(
            'idx-knowledgevideounit-Id',
            'knowledgevideounit',
            'Id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190811_202513_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190811_202513_init cannot be reverted.\n";

        return false;
    }
    */
}
